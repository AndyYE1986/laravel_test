<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\User\Subscription\SubscriptionController;
use App\Http\Controllers\User\Payment\PaymentController;
use App\Http\Controllers\User\Auth\RegisterController;
use App\Http\Controllers\User\Auth\LoginController;
use App\Http\Controllers\User\Article\ArticleController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/registration', [RegisterController::class, 'registration']);
Route::post('/login', [LoginController::class, 'login']);

Route::group(['middleware'=>['auth:api']], function() {
    Route::resource('/subscriptions', SubscriptionController::class);
    Route::resource('/articles', ArticleController::class);
    Route::post('/pay', [PaymentController::class, 'pay']);
});
