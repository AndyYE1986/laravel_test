<?php

namespace App\Action\Admin;

use App\Models\Admin;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Exceptions\JWTException;

class LoginAction
{
    public function __invoke(array $data): array
    {
        $admin = Admin::where('email', $data['email'])->firstOrFail();

        if (!Hash::check($data['password'], $admin->password)) {
            throw new \DomainException('invalid_credentials');
        }

        try {
            if (!$token = auth()->guard('admin')->attempt($data)) {
                throw new \DomainException('invalid_credentials');
            }
        } catch (JWTException $e) {
            throw new \DomainException('invalid_credentials');
        }

        return ['token' => $token, 'admin' => $admin];
    }
}
