<?php

namespace App\Action\Admin\Subscription;


use App\Models\Subscription;

class CreateAction
{
    public function __invoke(array $data)
    {
        return Subscription::create($data);
    }
}
