<?php

namespace App\Action\Admin\Subscription;


use App\Models\Subscription;

class UpdateAction
{
    public function __invoke(int $id, array $data)
    {
        Subscription::findOrFail($id)->update($data);

        return Subscription::findOrFail($id);
    }
}
