<?php

namespace App\Action\User\Payment;

use App\Models\PaymentSystem;
use App\Models\UserPayment;
use App\Models\UserSubscription;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class PayAction
{
    public function __invoke(array $data)
    {
        $userSubscribe = UserSubscription::find($data['user_subscription_id']);
        $paymentSystem = PaymentSystem::find($data['payment_system_id']);

        $service = App::make("App\Services\Payment\\".ucfirst($paymentSystem->name));

        /*TODO для простоти оплата завжди буде успішною*/
        $dataPay = $service->pay($userSubscribe->subscription->amount);

        try {

            DB::beginTransaction();

            $userSubscribe->update(['amount' => $userSubscribe->subscription->amount,
                                    'article_number' => $userSubscribe->subscription->article_number,
                                    'active_until' => Carbon::now()->addMonth()]);

            UserPayment::create([
               'user_subscription_id' =>  $data['user_subscription_id'],
                'payment_system_id' => $data['payment_system_id'],
                'paid' => $userSubscribe->subscription->amount,
                'payment_id' => $dataPay['paymentId'],
                'status' => $dataPay['status']
            ]);

            DB::commit();

        }catch (\Exception $exception){
            throw new \DomainException('error_create_payment');
        }

        return $userSubscribe->refresh();
    }
}
