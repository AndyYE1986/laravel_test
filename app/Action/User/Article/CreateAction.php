<?php

namespace App\Action\User\Article;

use App\Action\User\Subscription\GetActiveAction;
use App\Models\Article;


class CreateAction
{
    public function __invoke(array $data, GetActiveAction $activeAction)
    {
        $activeUserSubscription = $activeAction($data['user_id']);

        if (!$activeUserSubscription) {
            throw new \DomainException('you_dont_have_active_subscription');
        }

        $articles = Article::query()->where('user_subscription_id', $activeUserSubscription->id)->count();

        if ($articles >= $activeUserSubscription->article_number) {
            throw new \DomainException('your_limit_on_publishing_articles_has_been_exhausted');
        }

        return Article::create([
            'title'                => $data['title'],
            'body'                 => $data['body'],
            'user_subscription_id' => $activeUserSubscription->id
        ]);
    }
}
