<?php

namespace App\Action\User\Subscription;


use App\Models\Subscription;
use App\Models\UserPayment;
use App\Models\UserSubscription;
use Carbon\Carbon;


class isActiveAction
{
    public function __invoke(int $userId, int $subscriptionId)
    {
        $activeUserSubscription = UserSubscription::query()
            ->where('active_until', '>', Carbon::now())
            ->where('user_id', $userId)
            ->whereHas('payments', function($query){
                $query->where('status', UserPayment::STATUS_COMPLETED);
            })->exists();

        if($activeUserSubscription){
            throw new \DomainException('you_have_active_subscription');
        }

        $subscription = Subscription::find($subscriptionId);

        if(!$subscription->is_active){
            throw new \DomainException('subscription_not_available');
        }

        return true;
    }
}
