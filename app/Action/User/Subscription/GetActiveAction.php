<?php

namespace App\Action\User\Subscription;

use App\Models\UserPayment;
use App\Models\UserSubscription;
use Carbon\Carbon;

class GetActiveAction
{
    public function __invoke(int $userId)
    {
        return UserSubscription::query()
            ->where('active_until', '>', Carbon::now())
            ->where('user_id', $userId)
            ->whereHas('payments', function($query){
                $query->where('status', UserPayment::STATUS_COMPLETED);
            })->first();
    }
}
