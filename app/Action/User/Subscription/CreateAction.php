<?php

namespace App\Action\User\Subscription;


use App\Models\UserSubscription;
use Illuminate\Support\Facades\DB;

class CreateAction
{
    public function __invoke(array $data)
    {

        try {
            DB::beginTransaction();

            UserSubscription::query()
                ->where('user_id', $data['user_id'])
                ->where('subscription_id', $data['subscription_id'])
                ->whereDoesntHave('payments')->delete();

            $userSubscription = UserSubscription::create($data);

            DB::commit();

            return $userSubscription;

        }catch (\Exception $exception){
            throw new \DomainException('error_create_subscription');
        }
    }
}
