<?php

namespace App\Action\User\Auth;


use App\Models\User;
use Illuminate\Support\Facades\Hash;

class RegisterAction
{
    public function __invoke(array $data)
    {
        $data['password'] = Hash::make($data['password']);

        return User::create($data);
    }
}
