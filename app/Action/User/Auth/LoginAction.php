<?php

namespace App\Action\User\Auth;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Exceptions\JWTException;

class LoginAction
{
    public function __invoke(array $data)
    {
        $user = User::where('email', $data['email'])->firstOrFail();

        if (!Hash::check($data['password'], $user->password)) {
            throw new \DomainException('invalid_credentials');
        }

        try {
            if (!$token = auth()->guard('api')->attempt($data)) {
                throw new \DomainException('invalid_credentials');
            }
        } catch (JWTException $e) {
            throw new \DomainException('invalid_credentials');
        }

        return ['token' => $token, 'user' => $user];
    }
}
