<?php

namespace App\Services\Payment;

interface Payment
{
    public function pay(float $amount);
}
