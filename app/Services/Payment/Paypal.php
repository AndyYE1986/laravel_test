<?php

namespace App\Services\Payment;

use App\Models\UserPayment;
use Illuminate\Support\Str;


class Paypal implements Payment
{
    public function pay($amount)
    {
        return ['status' => UserPayment::STATUS_COMPLETED, 'paymentId' => Str::random()];
    }
}
