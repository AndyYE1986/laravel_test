<?php

namespace App\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    public const API_MIDDLEWARE = 'api';

    /**
     * Base api prefix
     *
     * @var string
     */
    public const API_PREFIX = 'api';


    public function map()
    {
        $this->mapAdminApiRoutes();
        $this->mapApiRoutes();
    }

    protected function mapApiRoutes()
    {
        Route::prefix(self::API_PREFIX)
            ->middleware(self::API_MIDDLEWARE)
            ->namespace($this->namespace)
            ->group(base_path('routes/api.php'));
    }


    protected function mapAdminApiRoutes()
    {
        Route::prefix(self::API_PREFIX . '/admin')
            ->middleware(self::API_MIDDLEWARE)
            ->group(base_path('routes/api.admin.php'));
    }

    /**
     * Configure the rate limiters for the application.
     *
     * @return void
     */
    protected function configureRateLimiting()
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(60)->by($request->user()?->id ?: $request->ip());
        });
    }
}
