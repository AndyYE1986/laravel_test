<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PaymentSystem
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentSystem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentSystem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentSystem query()
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentSystem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentSystem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentSystem whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentSystem whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class PaymentSystem extends Model
{
    protected $table = 'payment_systems';
    protected $fillable = [
        'name'
    ];

}
