<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UserPayment
 *
 * @property int $id
 * @property int $user_subscription_id
 * @property int $payment_system_id
 * @property float $paid
 * @property string $payment_id
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|UserPayment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserPayment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserPayment query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserPayment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserPayment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserPayment wherePaid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserPayment wherePaymentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserPayment wherePaymentSystemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserPayment whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserPayment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserPayment whereUserSubscriptionId($value)
 * @mixin \Eloquent
 */
class UserPayment extends Model
{
    protected $table = 'user_payments';

    public const STATUS_COMPLETED = 'completed';
    public const STATUS_PENDING = 'pending';
    protected $fillable = [
        'user_subscription_id',
        'payment_system_id',
        'paid',
        'payment_id',
        'status'
    ];

}
