<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Action\Admin\LoginAction;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\LoginRequest;

class LoginController extends Controller
{
    public function login(LoginRequest $request, LoginAction $loginAction): \Illuminate\Http\Response|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory
    {
        $data = $request->validated();

        return response($loginAction($data));
    }
}
