<?php

namespace App\Http\Controllers\Admin\Subscription;

use App\Action\Admin\Subscription\CreateAction;
use App\Action\Admin\Subscription\UpdateAction;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Subscription\SubscriptionCreateRequest;
use App\Http\Requests\Admin\Subscription\SubscriptionUpdateRequest;
use App\Models\Subscription;

class SubscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(Subscription::query()->latest()->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubscriptionCreateRequest $request, CreateAction $createAction)
    {
        $data = $request->validated();

        return response($createAction($data));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response(Subscription::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SubscriptionUpdateRequest $request, UpdateAction $updateAction, $id)
    {
        $data = $request->validated();

        return response($updateAction($id, $data));
    }
}
