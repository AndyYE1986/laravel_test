<?php

namespace App\Http\Controllers\User\Auth;

use App\Action\User\Auth\RegisterAction;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\Auth\RegisterRegisterRequest;

class RegisterController extends Controller
{
    public function registration(RegisterRegisterRequest $request, RegisterAction $registerAction)
    {

        $data = $request->validated();

        return response($registerAction($data));
    }
}
