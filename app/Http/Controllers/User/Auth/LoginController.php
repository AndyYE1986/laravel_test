<?php

namespace App\Http\Controllers\User\Auth;

use App\Action\User\Auth\LoginAction;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\Auth\LoginLoginRequest;

class LoginController extends Controller
{
    public function login(LoginLoginRequest $request, LoginAction $loginAction): \Illuminate\Http\Response|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory
    {
        $data = $request->validated();

        return response($loginAction($data));
    }
}
