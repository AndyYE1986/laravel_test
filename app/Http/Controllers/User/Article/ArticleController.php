<?php

namespace App\Http\Controllers\User\Article;

use App\Action\User\Article\CreateAction;
use App\Action\User\Subscription\GetActiveAction;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\Article\ArticleStoreRequest;
use App\Models\Article;
use App\Models\UserSubscription;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userSubscription = UserSubscription::where('user_id', auth()->id())->pluck('id')->toArray();

        return response(Article::query()
            ->whereIn('user_subscription_id', $userSubscription)
            ->latest()
            ->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleStoreRequest $request, CreateAction $createAction, GetActiveAction $activeAction)
    {
        $data = $request->validated();
        $data['user_id'] = auth()->id();

        return response($createAction($data, $activeAction));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $userSubscription = UserSubscription::where('user_id', auth()->id())->pluck('id')->toArray();

        return response(Article::query()
            ->whereIn('user_subscription_id', $userSubscription)
            ->where('id', $id)
            ->latest()
            ->get());
    }
}
