<?php

namespace App\Http\Controllers\User\Payment;

use App\Action\User\Payment\PayAction;
use App\Action\User\Subscription\isActiveAction;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\Payment\PaymentPayRequest;
use App\Models\UserSubscription;

class PaymentController extends Controller
{
    public function pay(PaymentPayRequest $request, PayAction $payAction, isActiveAction $isActiveAction)
    {
        $data = $request->validated();

        $userSubscribe = UserSubscription::find($data['user_subscription_id']);

        $isActiveAction($userSubscribe->user_id, $userSubscribe->subscription_id);

        return response($payAction($data));
    }
}
