<?php

namespace App\Http\Controllers\User\Subscription;

use App\Action\User\Subscription\isActiveAction;
use App\Action\User\Subscription\CreateAction;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\Subscription\SubscriptionCreateRequest;
use App\Models\UserSubscription;

class SubscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(UserSubscription::query()->with('payments')
            ->where('user_id', auth()->id())
            ->latest()
            ->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubscriptionCreateRequest $request, CreateAction $createAction, isActiveAction $isActiveAction)
    {
        $data = $request->validated();
        $data['user_id'] = auth()->id();

        $isActiveAction($data['user_id'], $data['subscription_id']);

        return response($createAction($data));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response(UserSubscription::query()
            ->with('payments')
            ->where('id',$id)
            ->where('user_id', auth()->id())->first());
    }
}
