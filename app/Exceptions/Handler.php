<?php

namespace App\Exceptions;

use App\Traits\FormatsErrorResponse;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Throwable;

class Handler extends ExceptionHandler
{
    use FormatsErrorResponse;
    /**
     * A list of exception types with their corresponding custom log levels.
     *
     * @var array<class-string<\Throwable>, \Psr\Log\LogLevel::*>
     */
    protected $levels = [
        //
    ];

    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<\Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $exception) {
            if (app()->bound('sentry') && $this->shouldReport($exception)) {
                app('sentry')->captureException($exception);
            }
        });
    }

    public function render($request, Throwable $exception)
    {
        if ($exception instanceof \Illuminate\Validation\ValidationException) {
            return response($this->errorResponse($exception->errors()), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        if ($exception instanceof \Illuminate\Auth\AuthenticationException) {
            return response($this->errorResponse('unauthenticated'), Response::HTTP_UNAUTHORIZED);
        }

        if ($exception instanceof \Tymon\JWTAuth\Exceptions\JWTException) {
            return response($this->errorResponse('unauthenticated'), Response::HTTP_UNAUTHORIZED);
        }

        if ($exception instanceof \Illuminate\Database\Eloquent\ModelNotFoundException) {
            return response($this->errorResponse('model_not_found'), Response::HTTP_NOT_FOUND);
        }

        if ($exception instanceof \Illuminate\Auth\Access\AuthorizationException) {
            return response($this->errorResponse('unauthorized'), Response::HTTP_FORBIDDEN);
        }

        if ($exception instanceof \Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException) {
            return response($this->errorResponse($exception->getMessage()), Response::HTTP_NOT_FOUND);
        }

        if ($exception instanceof \DomainException) {
            return response($this->errorResponse($exception->getMessage()), Response::HTTP_BAD_REQUEST);
        }

        if ($exception instanceOf \Illuminate\Http\Exceptions\ThrottleRequestsException) {
            return response($this->errorResponse($exception->getMessage()), Response::HTTP_TOO_MANY_REQUESTS);
        }

        if ($exception instanceof NotFoundHttpException) {
            return response($this->errorResponse('http_not_found'), Response::HTTP_NOT_FOUND);
        }

        if ($exception instanceof RouteNotFoundException) {
            $message = $exception->getMessage();
            if(strpos($message, 'login') > 0){
                $message = 'token_not_validate';
            }

            return response($this->errorResponse($message), Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return parent::render($request, $exception);
    }
}
