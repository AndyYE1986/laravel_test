1) git clone https://gitlab.com/AndyYE1986/laravel_test.git
4) створити файл .env та скопіювати з .env.dev
5) docker run --rm --interactive --tty \
   --volume $PWD:/app \
   composer install
6) docker compose up
7) sudo chmod -R ugo+rw storage
8) docker-compose exec app php artisan migrate:fresh --seed
