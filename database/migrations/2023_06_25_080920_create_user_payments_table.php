<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_payments', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('user_subscription_id');
            $table->unsignedBigInteger('payment_system_id');

            $table->float('paid', 8,2);
            $table->string('payment_id');
            $table->string('status')->default(\App\Models\UserPayment::STATUS_PENDING);

            $table->timestamps();

            $table->foreign('user_subscription_id')->references('id')->on('user_subscriptions')->onDelete('cascade');
            $table->foreign('payment_system_id')->references('id')->on('payment_systems')->onDelete('cascade');

            $table->index('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_payments');
    }
};
