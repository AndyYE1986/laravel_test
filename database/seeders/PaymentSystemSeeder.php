<?php

namespace Database\Seeders;

use App\Models\Admin;
use App\Models\PaymentSystem;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class PaymentSystemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PaymentSystem::query()->insert([
            ['name' => 'paypal'],
            ['name' => 'stripe']
        ]);
    }
}
